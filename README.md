# Music Transcription Docker Compose Help
This codebase contains a docker-compose that can
To build use:
dd
```bash
docker-compose build
```

Example of usage starting from a Youtube video:

```bash
docker-compose run youtube-dl --extract-audio --audio-format mp3 "https://www.youtube.com/watch?v=DPniE-OR9hs"

docker-compose run demucs -n mdx_extra_q -d cpu "Lo spirito del Signore-DPniE-OR9hs.mp3" -o .

# use the features that flute are similar to voice
docker-compose run spleeter separate -o output "/output/mdx_extra_q/Lo spirito del Signore-DPniE-OR9hs/other.wav"

docker-compose run omnizart music transcribe "mdx_extra_q/Lo spirito del Signore-DPniE-OR9hs/other.wav"
```
